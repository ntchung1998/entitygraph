import tensorflow as tf
import numpy as np
import scipy
import scipy.spatial.distance

def masked_softmax_cross_entropy(preds, labels, mask):
    """Softmax cross-entropy loss with masking."""
    loss = tf.nn.softmax_cross_entropy_with_logits(logits=preds, labels=labels)
    mask = tf.cast(mask, dtype=tf.float32)
    mask /= tf.reduce_mean(mask)
    loss *= mask
    return tf.reduce_mean(loss)


def masked_accuracy(preds, labels, mask):
    """Accuracy with masking."""
    correct_prediction = tf.equal(tf.argmax(preds, 1), tf.argmax(labels, 1))
    accuracy_all = tf.cast(correct_prediction, tf.float32)
    mask = tf.cast(mask, dtype=tf.float32)
    mask /= tf.reduce_mean(mask)
    accuracy_all *= mask
    return tf.reduce_mean(accuracy_all)


def get_placeholder_by_name(name):
    try:
        return tf.get_default_graph().get_tensor_by_name(name+":0")
    except:
        return tf.placeholder(tf.int32, name=name)


def align_loss(outlayer, ILL, gamma, k):
    left = ILL[:, 0]
    right = ILL[:, 1]
    t = len(ILL)
    left_x = tf.nn.embedding_lookup(outlayer, left)
    right_x = tf.nn.embedding_lookup(outlayer, right)
    A = tf.reduce_sum(tf.abs(left_x - right_x), 1)
    neg_left = get_placeholder_by_name("neg_left") #tf.placeholder(tf.int32, [t * k], "neg_left")
    neg_right = get_placeholder_by_name("neg_right") #tf.placeholder(tf.int32, [t * k], "neg_right")
    neg_l_x = tf.nn.embedding_lookup(outlayer, neg_left)
    neg_r_x = tf.nn.embedding_lookup(outlayer, neg_right)
    B = tf.reduce_sum(tf.abs(neg_l_x - neg_r_x), 1)
    C = - tf.reshape(B, [t, k])
    D = A + gamma
    L1 = tf.nn.relu(tf.add(C, tf.reshape(D, [t, 1])))
    neg_left = get_placeholder_by_name("neg2_left") #tf.placeholder(tf.int32, [t * k], "neg2_left")
    neg_right = get_placeholder_by_name("neg2_right") #tf.placeholder(tf.int32, [t * k], "neg2_right")
    neg_l_x = tf.nn.embedding_lookup(outlayer, neg_left)
    neg_r_x = tf.nn.embedding_lookup(outlayer, neg_right)
    B = tf.reduce_sum(tf.abs(neg_l_x - neg_r_x), 1)
    C = - tf.reshape(B, [t, k])
    L2 = tf.nn.relu(tf.add(C, tf.reshape(D, [t, 1])))
    return (tf.reduce_sum(L1) + tf.reduce_sum(L2)) / (2.0 * k * t)

def get_precisions(vec, test_pair, top_k=(1, 5, 10, 20, 30, 50, 100)):
    pass

def get_hits(vec, test_pair, top_k=(1, 10, 20, 50, 100)):
    Lvec = np.array([vec[e1] for e1, e2 in test_pair])
    Rvec = np.array([vec[e2] for e1, e2 in test_pair])
    t_len = len(test_pair)
    sim = scipy.spatial.distance.cdist(Lvec, Rvec, metric='cosine')
    top_lr = [0] * len(top_k)
    MAPl = 0
    Hitl = 0
    AUCl = 0
    for i in range(Lvec.shape[0]):
        rank = sim[i, :].argsort()
        rank_index = np.where(rank == i)[0][0]
        MAPl+= 1/(rank_index + 1)
        Hitl+= (t_len + 1) / (t_len)
        AUCl+= (t_len - rank_index -1)/(t_len - 1)
        for j in range(len(top_k)):
            if rank_index < top_k[j]:
                top_lr[j] += 1
    MAPl /= t_len
    AUCl /= t_len
    Hitl /= t_len
    top_rl = [0] * len(top_k)
    MAPr = 0
    Hitr = 0
    AUCr = 0
    for i in range(Rvec.shape[0]):
        rank = sim[:, i].argsort()
        rank_index = np.where(rank == i)[0][0]
        MAPr+= 1/(rank_index + 1)
        Hitr+= (t_len + 1) / (t_len)
        AUCr+= (t_len - rank_index -1)/(t_len - 1)
        for j in range(len(top_k)):
            if rank_index < top_k[j]:
                top_rl[j] += 1
    MAPr /= t_len
    AUCr /= t_len
    Hitr /= t_len
    print('For each left:')
    for i in range(len(top_lr)):
        print('Hit %d: %.2f%%' % (top_k[i], top_lr[i] / len(test_pair) * 100))
    print('MAP = {:.3f},AUC = {:.3f}'.format(MAPl,AUCl))
    print('For each right:')
    for i in range(len(top_rl)):
        print('Hit %d: %.2f%%' % (top_k[i], top_rl[i] / len(test_pair) * 100))
    print('MAP = {:.3f}, AUC = {:.3f}'.format(MAPr,AUCr))

def get_combine_hits(se_vec, ae_vec, beta, test_pair, top_k=(1, 5, 10, 20, 30, 50, 100)):
    vec = np.concatenate([se_vec*beta, ae_vec*(1.0-beta)], axis=1)
    get_hits(vec, test_pair, top_k)
