import numpy as np
import scipy


def get_hits(vec, test_pair, top_k=(1, 10, 50, 100)):
    Lvec = np.array([vec[e1] for e1, e2 in test_pair])
    Rvec = np.array([vec[e2] for e1, e2 in test_pair])
    t_len = len(test_pair)

    sim = scipy.spatial.distance.cdist(Lvec, Rvec, metric='cityblock')
    top_lr = [0] * len(top_k)
    MAPl = 0
    Hitl = 0
    AUCl = 0
    for i in range(Lvec.shape[0]):
        rank = sim[i, :].argsort()
        rank_index = np.where(rank == i)[0][0]
        MAPl+= 1/(rank_index + 1)
        Hitl+= (t_len + 1) / (t_len)
        AUCl+= (t_len - rank_index -1)/(t_len - 1)
        for j in range(len(top_k)):
            if rank_index < top_k[j]:
                top_lr[j] += 1
    MAPl /= t_len
    AUCl /= t_len
    Hitl /= t_len
    top_rl = [0] * len(top_k)
    MAPr = 0
    Hitr = 0
    AUCr = 0
    for i in range(Rvec.shape[0]):
        rank = sim[:, i].argsort()
        rank_index = np.where(rank == i)[0][0]
        MAPr+= 1/(rank_index + 1)
        Hitr+= (t_len + 1) / (t_len)
        AUCr+= (t_len - rank_index -1)/(t_len - 1)
        for j in range(len(top_k)):
            if rank_index < top_k[j]:
                top_rl[j] += 1
    MAPr /= t_len
    AUCr /= t_len
    Hitr /= t_len
    print('For each left:')
    for i in range(len(top_lr)):
        print('Hits@%d: %.2f%%' % (top_k[i], top_lr[i] / len(test_pair) * 100))
    print('MAPlr = {:.3f}, Hitlr = {:.3f},AUClr = {:.3f}'.format(MAPl,Hitl,AUCl))
    print('For each right:')
    for i in range(len(top_rl)):
        print('Hits@%d: %.2f%%' % (top_k[i], top_rl[i] / len(test_pair) * 100))
    print('MAPrl = {:.3f}, Hitrl = {:.3f},AUClrl = {:.3f}'.format(MAPr,Hitr,AUCr))

